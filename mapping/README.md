# Mapping Files

The files in this directory provide a mapping from a content id (expressed as ClearlyDefined coordinates) to the source of information ("authority") that we have available for that content. For convenience, it also includes the license as we understand it and status (`approved` or `restricted`).

This information is used to build a database that is used by the Eclipse Dash License Tool backend to evaluate content. The content from every file with the suffix `.cvs` is used.

Sources of information use these patterns:

* a hashmark followed by a number points to an issue from this repository, e.g., \#999;
* the letters `CQ` followed by the number of a contribution questionnaire in IPZilla, e.g., CQ1234; or
* the literal `clearlydefined` indicating that the source of license information is ClearlyDefined.

Every file must have a header in the first line, containing values "id", "license", "status", and "authority". The order of the headers is not significant, but must be reflected in the rest of the document.
