# The Eclipse Foundation's Intellectual Property Lab (IPLab)

The Eclipse Foundation's IP Team uses this repository to track the vetting of intellectual property content on behalf of Eclipse project teams.

If your project leverages build technology that pulls third-party content from well-defined software repositories (e.g., Apache Maven pulling from Maven Central, a `yarn.lock` or `package-lock.json` file), consider using the [Eclipse Dash License Tool](https://github.com/eclipse/dash-licenses) to [automatically create](https://github.com/eclipse/dash-licenses#automatic-ip-team-review-requests) IPLab issues for you.

> Note that the **Eclipse Dash License Tool** is only capable of creating issues for content that it either discovers from your build or that you feed to it. It will not, for example, find third-party content that is buried in your code. If you've, for example, added some JavaScript library in a `lib` directory or similar, then you may have to manually [create a request](https://gitlab.eclipse.org/eclipsefdn/emo-team/iplab/-/issues/new?issuable_template=vet-third-party) to vet that content and manually track your project's use of that library.

If you cannot use the Eclipse Dash License Tool, you can manually create a request:

- [Create a request](https://gitlab.eclipse.org/eclipsefdn/emo-team/iplab/-/issues/new?issuable_template=vet-third-party) to vet Third-party content
- [Create a request](https://gitlab.eclipse.org/eclipsefdn/emo-team/iplab/-/issues/new?issuable_template=vet-project) to vet Project Content

More information about the Eclipse Dash License Tool:

- [Eclipse Dash License Tool](https://github.com/eclipse/dash-licenses) on GitHub
- [Eclipse Dash License Tool Maven Plugin](https://blog.waynebeaton.ca/posts/ip/dash-license-tool-maven-plugin/)
- [Eclipse Dash License Tool and The Maven Reactor](https://blog.waynebeaton.ca/posts/ip/dash-license-tool-maven-reactor)
- [Eclipse Dash License Tool and Package Lock Files](https://blog.waynebeaton.ca/posts/ip/dash-license-tool-package-lock/)

Other Links:

- [Mapping Directory](https://gitlab.eclipse.org/eclipsefdn/emo-team/iplab/-/wikis/Mapping-Directory)
- [Curation](https://gitlab.eclipse.org/eclipsefdn/emo-team/iplab/-/wikis/Curation)
