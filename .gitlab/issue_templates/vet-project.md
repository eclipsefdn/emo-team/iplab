<!--
This template is intended to help you set up a request to have the
Eclipse IP Team review project content. Project content is defined
as content that is to be maintained by the Eclipse project team. This
could, for example, be an entire new Git repository, or a merge or pull
request for an existing repository.

Please see the Eclipse Foundation Project Handbook for more information.

https://www.eclipse.org/projects/handbook/#ip-project-content

The Eclipse IP Team has provided some comments in this issue to guide
you to provide the information that we need to engage with you in this
review. Please provide as much information as you can. If information is
missing, or you are not certain how information should be provided,
provide what you can and the IP Team will work with you to sort it out.

This issue uses markdown, so use Markdown syntax when specifying
information.

INSTRUCTIONS
============

Set the title of the issue this pattern:

"project/<project id>/-/<name>/0.0"

Where <project id> is your Eclipse project's id (e.g., technology.dash)
and <name> is a technical name (that makes sense to you) for your contribution. 
The technical name might, for example, be the name of the repository. Don't
include spaces in the technical name.

e.g., project/technology.dash/-/license-server/0.0

Issues that are created with a well-formed identifier will automatically 
processed and so will likely be resolved quickly.

We need you to identify the Eclipse project that is accepting this contribution.

In the template below, replace <project name> with the name of your Eclipse 
project (e.g., Eclipse Dash) and <project id> with the project's id
(e.g., technology.dash).

In the Basic Information section, provide as much information as you can, 
including the license of the content as you understand it as an SPDX 
identifier (https://spdx.org/licenses/), and URLs that points to the binary 
and sources of the content. 

We need pointers to specific versions of content, ideally in an 
archive format (e.g. ZIP file). That is, please provide a pointer to 
a specific downloadable ZIP archive associated with a particular 
release or the entire repository.

On GitHub, for example, you can send the "Download ZIP" link from the 
"Code" dropdown on an existing repository. If you're not sure, provide
a link to the contribution and the IP Team will work with you to 
sort it out.

NOTE that we need to have a source pointer. The value that you provide
should point to the content in an archive format (e.g., a "zip" file)
If you cannot (or do not) provide it, processing will be delayed while 
we hunt it down.

NOTE that the specific form is important, particularly with regard
to making it so that the automation can actually find the source.

Please provide any additional information that you think might be useful to
assist the IP Team in resolving this issue in free form text after the 
Basic Information.

These comments can be deleted before you submit (or not, whatever).
-->

Project: [<project name>](https://projects.eclipse.org/projects/<project id>)

# Basic Information
  - License: 
  - Copyright Holder:
  - [Git repository]()
  - [Binary]()
  - [Source]()
  
/label ~"Review Needed" ~"Project Content"
