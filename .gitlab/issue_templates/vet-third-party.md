<!--
This template is intended to help you set up a request to have the
Eclipse IP Team review third-party content. Third-party content is defined
as content that is leveraged or otherwise referenced by project code.
Third-party content is typically acquired from software repositories
(e.g., Maven Central or NPMJS) during builds.

Please see the Eclipse Foundation Project Handbook for more information.

https://www.eclipse.org/projects/handbook/#ip-third-party

The Eclipse IP Team has provided some comments in this issue to guide
you to provide the information that we need to engage with you in this
review. Please provide as much information as you can. If information is
missing, or you are not certain how information should be provided,
provide what you can and the IP Team will work with you to sort it out.

NOTE Consider using the Eclipse Dash License Tool to automate this
the creation of issues for third-party content.

https://github.com/eclipse/dash-licenses

This issue uses markdown, so use Markdown syntax when specifying
information.

INSTRUCTIONS
============

Provide either a meaningful (text) title or a content identifier.

If you set the title to one of the following formats, the scanner _should_ be able
to find the source for your request from that:

* ClearlyDefined ID: {type}/{source}/{namespace}/{name}/{revision}
* Maven GAV: {groupid}:{artifactid}:{revision}
* Package URL: pkg:{type}/{namespace}/{name}@{revision}
* GitHub Commit URL: https://github.com/{org}/{project}/commit/{ref}
* GitHub Pull request URL: https://github.com/{org}/{project}/pull/{id}

If you use any of these formats, IPLab will use the information 
provided to try and locate the source. If you add an explicit 
[Source]() reference in the description, it'll use that instead.

If you're not sure, do your best and the IP Team will assist.

Issues that correctly use these forms for the title and source
pointer (see below) will be automatically processed and so will 
likely be resolved quickly.

We need you to identify the Eclipse project that is accepting this contribution.

In the template below, replace <project name> with the name of your Eclipse 
project (e.g., Eclipse Dash) and <project id> with the project's id
(e.g., technology.dash).

In the Basic Information section, provide as much information as you can, 
including the license of the content as you understand it as an SPDX 
identifier (https://spdx.org/licenses/), and URLs that points to the binary 
and sources of the content. 

We need pointers to specific versions of content, ideally in an 
archive format (e.g. ZIP file). That is, please do not provide a 
pointer to a repository on GitHub; rather, provide a pointer to 
a specific downloadable ZIP archive associated with the particular 
release.

NOTE that we need to have a source pointer. If you cannot (or do not)
provide it, processing will be delayed while we hunt it down.

Please provide any additional information that you think might be useful to
assist the IP Team in resolving this issue in free form text after the 
Basic Information.

These comments can be deleted before you submit (or not, whatever).
-->

Project: [<project name>](https://projects.eclipse.org/projects/<project id>)


# Basic Information
  - License: 
  - Copyright Holder:
  - [Git repository]()
  - [Binary]()
  - [Source]()

/label ~"Review Needed" ~"Third Party Content"
