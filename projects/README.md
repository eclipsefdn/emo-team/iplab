# Projects

This directory contains information about projects.

The `namespace.hints` file provides _hints_ of the mapping between package patterns and Eclipse open source projects. The gist is  to provide a means of hinting information that we cannot otherwise derive from other sources.
